﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionCabeza : MonoBehaviour {

    // Use this for initialization
    void Start() {
        Input.gyro.enabled = true; // Enable gyroscope
    }

    // Update is called once per frame
    void Update() {
        Quaternion lookAt = Input.gyro.attitude;
        lookAt = Quaternion.Euler(90f, 0f, 0f) * new Quaternion(lookAt.x, lookAt.y, -lookAt.z, -lookAt.w);
        transform.rotation = lookAt;
    }
}
